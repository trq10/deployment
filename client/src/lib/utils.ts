import { ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";
import { AxiosResponse } from 'axios';
import type { Session, User } from 'next-auth'

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}


interface ApiResponse<T> {
  success: boolean;
  data: T;
  // Add other properties if needed
}


export const requestHandler = async <T>(
  api: () => Promise<AxiosResponse<ApiResponse<T>>>, // Use ApiResponse<T> here
  setLoading: ((loading: boolean) => void) | null,
  onSuccess: (data: T) => void,
  onError: (error: string) => void
) => {
  setLoading && setLoading(true);

  try {
    const response = await api();
    const { data } = response;

    if (data?.success) {
      onSuccess(data.data);
    }
  } catch (error: any) {
    if ([401, 403].includes(error?.response?.data?.statusCode)) {
      LocalStorage.clear();
      if (typeof window !== 'undefined') window.location.href = '/login';
    }
    onError(error?.response?.data?.message || 'Something went wrong');
  } finally {
    setLoading && setLoading(false);
  }
};

export class LocalStorage {
  static get<T>(key: string): T | null {
    if (typeof window === 'undefined') return null;
    const value = localStorage.getItem(key);
    if (value) {
      try {
        return JSON.parse(value) as T;
      } catch (err) {
        return null;
      }
    }
    return null;
  }

  static set<T>(key: string, value: T): void {
    if (typeof window === 'undefined') return;
    localStorage.setItem(key, JSON.stringify(value));
  }

  static remove(key: string): void {
    if (typeof window === 'undefined') return;
    localStorage.removeItem(key);
  }

  static clear(): void {
    if (typeof window === 'undefined') return;
    localStorage.clear();
  }
}


export default function convertToBase64(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      resolve(fileReader.result as string);
    };

    fileReader.onerror = (error) => {
      reject(error);
    };
  });
}

export const isAuthenticated = (): boolean => {
  const token = localStorage.getItem('token');
  if (!token) {
    return false
  }
  else {
    return true;
  }
}

export function encontrarDiferencas(array1: [], array2: User[]) {
  if (!Array.isArray(array1) || !Array.isArray(array2)) {
    throw new Error("Ambos os parâmetros devem ser arrays.");
  }

  if (array1.length === 0) {
    return array2.slice(); // Se array1 estiver vazio, retorna todos os elementos de array2
  }

  if (array2.length === 0) {
    return array1.slice(); // Se array2 estiver vazio, retorna todos os elementos de array1
  }

  const diferenca1 = array1.filter(item => !array2.some(obj => JSON.stringify(obj) === JSON.stringify(item)));
  const diferenca2 = array2.filter(item => !array1.some(obj => JSON.stringify(obj) === JSON.stringify(item)));

  return [...diferenca1, ...diferenca2];
}